This module can take 2 variables. They should be defined in the site's settings.php file.

uw_auth_wcms_admins_ldap_group_admin (required):
Either a single string containing a single LDAP group, or an array of strings, where each item contains an LDAP group.
Logins from members of the defined group(s) will be assigned the role of "administrator" on login, unless they are defined in uw_auth_wcms_admins_forbidden_admins.

uw_auth_wcms_admins_forbidden_admins (optional):
Either a single string containing a single userID, or an array of strings, where each item contains a userID.
Logins from userIDs in this variable will not be assigned "administrator", even if they are a member of a defined LDAP group.
